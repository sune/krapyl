/*
    Copyright (c) 2011 Sune Vuorela <sune@vuorela.dk>

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE
*/

#include <QWidget>
#include <QLineEdit>
#include <QLabel>
#include <QFormLayout>
#include <QVBoxLayout>

#include <QSettings>

#include <QtCrypto/QtCrypto>

class VerifierGui : public QWidget {
  Q_OBJECT
  public:
    VerifierGui(QWidget* parent=0) : 
           QWidget(parent),
	   m_username(new QLineEdit),
	   m_secret(new QLineEdit),
	   m_vote_token(new QLineEdit),
	   m_result_label(new QLabel)
    {
        QCA::init();
        if(!QCA::isSupported("sha256")) {
	  qFatal("QCA doesn't support sha256");
	  return;
	}

        {
            QSettings s;
            m_username->setText(s.value("username",QString()).toString());
	}
        
        connect(m_username, SIGNAL(textChanged(QString)), this, SLOT(usernameChanged(QString)));

	connect(m_username, SIGNAL(textChanged(QString)), this, SLOT(calculate()));
	connect(m_secret, SIGNAL(textChanged(QString)), this, SLOT(calculate()));
	connect(m_vote_token, SIGNAL(textChanged(QString)), this, SLOT(calculate()));

	QVBoxLayout* layout = new QVBoxLayout();
	QLabel* topbar = new QLabel("Fancy graphics");
	topbar->setAlignment(Qt::AlignCenter);
	layout->addWidget(topbar);


        QFormLayout* lay = new QFormLayout();
        lay->addRow(new QLabel("username"), m_username);
	lay->addRow(new QLabel("secret"), m_secret);
	lay->addRow(new QLabel("vote token"), m_vote_token);
	layout->addLayout(lay);

	layout->addWidget(m_result_label);

	m_result_label->setTextInteractionFlags(Qt::TextSelectableByMouse|Qt::TextSelectableByKeyboard);

        setLayout(layout);
	calculate();
    }
  private Q_SLOTS:
    void calculate() {
        QCA::Hash shahash("sha256");
	shahash.update(m_username->text().toLocal8Bit());
	shahash.update(m_secret->text().toLocal8Bit());
	shahash.update(m_vote_token->text().toLocal8Bit());
	QByteArray hashResult = shahash.final().toByteArray();
	m_result_label->setText(QCA::arrayToHex(hashResult));
    }
    void usernameChanged(QString text) {
        QSettings s;
        s.setValue("username",text);
    }
  private:
    QLineEdit* m_username;
    QLineEdit* m_secret;
    QLineEdit* m_vote_token;
    QLabel* m_result_label;

};

